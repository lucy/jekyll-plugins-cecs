# jekyll-plugins-cecs

This repository contains the plugins we use in the CECS Jekyll theme.

If you want to learn how to make use of the CECS Jekyll theme, check out the course-in-a-box repository.
