module Jekyll
  module PresentFilter
    def present(input)
      return input if input.nil?

      input
        .select do |obj|
          if obj.respond_to?(:date)
            obj.date <= @context.registers[:site].time
          else
            true
          end
        end
    end
  end
end

Liquid::Template.register_filter(Jekyll::PresentFilter)
