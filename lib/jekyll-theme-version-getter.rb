# Jekyll plugin for getting the version information of the "jekyll-theme-cecs"
# plugin, and putting it into the site.data['theme-version'] variable.

module Jekyll
  class JekyllThemeVersionGetter < Generator
    priority :high
    safe true
    def generate(site)
      theme = Bundler.load.specs.detect {|gem| gem.name == "jekyll-theme-cecs"}
      site.data['theme-version'] = "#{theme.version} (#{theme.git_version.strip})"
    end
  end
end
