module Jekyll
  class SimpleBreadcrumb < Liquid::Tag
    def render(context)
      crumbs = []

      url = context['page']['url']
      segments = url.split('/')

      pages = []
        .concat(context['site']['pages'])
        .concat(context['site']['documents'])

      if url == "/"
        crumbs << { 'url' => '/', 'name' => 'Home'}
      else
        so_far = []

        for segment in segments
          looking_for = so_far.concat([segment])

          page = pages.find { |p| p.url.split('/') == looking_for }

          crumbs << { 'url' => page.url, 'name' => page.data['title'] } if page
        end
      end

      crumbs
        .map do |c|
          if c == crumbs.last
            c['name']
          else
            "<a href=\"#{context['site']['baseurl']}#{c['url']}\">#{c['name']}</a>"
          end
        end
        .join " » "
    end
  end
end

Liquid::Template.register_tag('breadcrumb', Jekyll::SimpleBreadcrumb)
