Jekyll::Hooks.register :site, :post_write do |site|
  log_prefix = "CECS plugin:"

  mode = site.config.dig("cecs", "html-proofer")

  unless [nil, "warn", "error"].include? mode
    Jekyll.logger.abort_with(log_prefix, "'cecs.html-proofer' must be either nil, 'warn', or 'error'. Instead, received: '#{mode}'")
  end

  next if mode.nil?

  # drop "/" from start and end of baseurl. HTML proofer does not like it being there.
  baseurl = site.config["baseurl"].chomp("/")

  if baseurl[0] == "/"
    baseurl = baseurl[1..-1]
  end

  begin
    HTMLProofer.check_directory(site.config["destination"], opts = {
      :alt_ignore => [
        /style.anu.edu.au/,
      ],
      :file_ignore => [
        /_site\/assets\/vendor/
      ],
      :allow_hash_href => true,
      :disable_external => true,
      :url_swap => {
        /#{baseurl}/ => "/",
      }
    }).run
  rescue RuntimeError => e
    if mode == "warn"
      Jekyll.logger.warn(log_prefix, e.message)
    elsif mode == "error"
      Jekyll.logger.abort_with(log_prefix, e.message)
    end
  end
end
