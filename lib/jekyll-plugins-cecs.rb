require "jekyll"
require "html-proofer"

require "nokogiri"

# other dependencies
require "jekyll/fontawesome/svg"
require "jekyll-feed"

require "simple_breadcrumb"
require "jekyll_proofer"
require "present"
require "jekyll-theme-version-getter"
