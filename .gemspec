Gem::Specification.new do |spec|
  spec.name          = "jekyll-plugins-cecs"
  spec.version       = "2.0.0.beta"
  spec.authors       = ["Ben Swift", "Harrison Shoebridge"]
  spec.email         = ["helpdesk@cecs.anu.edu.au"]

  spec.summary       = "ANU CECS Theme for Jekyll Pages with gitlab"
  spec.homepage      = ""
  spec.license       = "Nonstandard"

  spec.files         = Dir['lib/*.rb']

  spec.add_runtime_dependency "jekyll"
  spec.add_runtime_dependency "html-proofer"
  spec.add_runtime_dependency "nokogiri"
  spec.add_runtime_dependency "jekyll-feed"
  spec.add_runtime_dependency "jekyll-fontawesome-svg"
end
